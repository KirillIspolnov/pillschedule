package org.kirillbogatikov.mypillschedule;

public class Time {
    private int hours, minutes;
    
    public Time(int h, int m) {
        this.hours = h;
        this.minutes = m;
    }
    
    public Time(double dec) {
        this();
        fromDecimalForm(dec);
    }
    
    public Time(String s) {
        String[] parts = s.split(":");
        this.hours = Integer.parseInt(parts[0]);
        this.minutes = Integer.parseInt(parts[1]);
    }
    
    public Time() {
        this(0, 0);
    }
    
    public int getHours() {
        return this.hours;
    }
    
    public int getMinutes() {
        return this.minutes;
    }
    
    public double toDecimalForm() {
        return hours + (minutes / 60.0);
    }
    
    public void fromDecimalForm(double dec) {
        this.hours = (int)Math.floor(dec);
        this.minutes = (int)(dec * 60) % 60;
    }
    
    public Time plus(Time other) {
        double dec = this.toDecimalForm() + other.toDecimalForm();
        return new Time(dec);
    }
    
    public Time minus(Time other) {
        double dec = this.toDecimalForm() - other.toDecimalForm();
        return new Time(dec);
    }
    
    public Time divide(int c) {
        return new Time(this.toDecimalForm() / c);
    }
    
    public Time multiple(int c) {
        return new Time(this.toDecimalForm() * c);
    }
    
    public boolean isLessThan(Time other) {
        return Time.min(this, other).equals(this);
    }
    
    public boolean isBiggerThan(Time other) {
        return Time.max(this, other).equals(this);
    }
    
    public String toString() {
        StringBuilder b = new StringBuilder();
        
        if(hours < 10)
            b.append(0);
        b.append(hours).append(":");
        
        if(minutes < 10)
            b.append(0);
        b.append(minutes);
        
        return b.toString();
    }
    
    public static Time max(Time a, Time b) {
        if(a.toDecimalForm() > b.toDecimalForm())
            return a;
        return b;
    }
    
    public static Time min(Time a, Time b) {
        if(a.toDecimalForm() < b.toDecimalForm())
            return a;
        return b;
    }
}
