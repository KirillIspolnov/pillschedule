package org.kirillbogatikov.mypillschedule;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class MySchedule {
    private static final Time STEP = new Time(0, 15);
    
    static Scanner scanner = new Scanner(System.in);
    
    public static void main(String[] args) {
        System.out.println("Доброго времени суток! Я помогу Вам составить правильное расписание для приёма таблеток! Выберите удобный способ воода данных:");
        System.out.println("По умолчанию, я получаю от Вас данные с клавиатуры, однако введя ниже путь к некоторому файлу, Вы сможете уговорить меня читать нужные мне данные из файла: ");
        String data = scanner.nextLine();
        if(!data.isEmpty()) {
            System.out.println("Я Вас понял. Пробую найти и прочитать нужный файл...");
            try {
                scanner = new Scanner(new File(data));
            } catch(IOException e) {
                System.out.println("Ой, кажется что-то  пошло не так. Мне придётся аварийно прекратить работу. Вот данные об ошибке: ");
                System.out.println(e);
                return;
            }
            System.out.println("Всё хорошо! Читаю данные из файла!");
        }
        
        System.out.print("Скажите, во сколько вы завтракаете: ");
        Time breakfast = new Time(scanner.nextLine());
            
        System.out.print("Скажите, во сколько вы ужинаете: ");
        Time supper = new Time(scanner.nextLine());
        
        System.out.print("Скажите, какой интервал между приёмами таблеток нужно соблюдать: ");
        Time tabletInterval = new Time(scanner.nextLine());
        
        System.out.print("Скажите, а за сколько до приёма еды нужно принять таблетку: ");
        Time before = new Time(scanner.nextLine());
        
        Time available = supper.minus(breakfast);
        
        Time lunch = breakfast.plus(available.divide(2));
        
        boolean changeSchedule = false;
        while(supper.minus(breakfast).isBiggerThan(tabletInterval)) {
            changeSchedule = true;
            breakfast = breakfast.plus(STEP);
            supper = supper.minus(STEP);
        }
        
        if(changeSchedule) {
            System.out.println("Похоже, Ваш привычный распорядок дня изменится: ");
        }
        
        System.out.printf("Первый приём таблеток: %s%n", breakfast.minus(before));
        System.out.printf("Завтрак: %s%n", breakfast);
        System.out.printf("Обед: %s%n", lunch);
        System.out.printf("Второй приём таблеток: %s%n", supper.minus(before)); 
        System.out.printf("Ужин: %s%n", supper);
    }

}
